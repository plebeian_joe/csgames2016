#!/usr/bin/python3.4

# code taken from http://stackoverflow.com/questions/3025162/statistics-combinations-in-pythom
def choose(n, k):
    """
    A fast way to calculate binomial coefficients by Andrew Dalke (contrib).
    """
    if 0 <= k <= n:
        ntok = 1
        ktok = 1
        for t in range(1, min(k, n - k) + 1):
            ntok *= n
            ktok *= t
            n -= 1
        return ntok // ktok
    else:
        return 0

# counting the number of anagrams from single letters...
def anagrams_from_single_letters(string):
    nb_anagram = 0
    char_in_string = {}
    for char in string:
        if char in char_in_string:
            char_in_string[char] += 1
        else:
            char_in_string[char] = 1

    for char in char_in_string.keys():
        nb_repetition = char_in_string[char]
        if(nb_repetition > 1):
            nb_anagram += choose(nb_repetition, 2)
    
    return nb_anagram


# determine if the two strings are anagram to each other
def is_anagram(str1, str2):
    str1_cp = list(str1)
    str2_cp = list(str2)
    str1_cp.sort()
    str2_cp.sort()
    return str1_cp == str2_cp


nb_str = input()
answer = []

# main algorithm:
#       1. counts the anagrams from the single letters
#       2. counts the anagrams for every substring possible
for i in range(int(nb_str)):
    string = input()
    str_len = len(string)
    nb_anagram = anagrams_from_single_letters(string)
    for i in range(str_len):
        for j in range(i + 1, str_len):
            substr1 = string[i:j+1]
            for k in range(i + 1, str_len - len(substr1) + 1):
                substr2 =  string[k:k + len(substr1)]
                if is_anagram(substr1, substr2):
                    nb_anagram += 1
    answer.append(nb_anagram)



# printing the number of «unordered anagrammatic pairs»
for ans in answer:
    print(ans)

