#!/usr/bin/python3.4

puzzle = "1113222113"

for i in range(40):
    temp = ""
    digit = puzzle[0]
    nb = 0
    for char in puzzle:
        if char != digit:
            temp = temp + (str(nb))
            temp = temp + digit
            nb = 1
            digit = char
        else:
            nb += 1
    temp = temp + (str(nb))
    temp = temp + digit
    nb = 1
    digit = char

    puzzle = temp

print(len(puzzle))
