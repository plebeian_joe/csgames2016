#!/usr/bin/python3.4
import hashlib

puzzle = "bgvyzdsv"

m = hashlib.md5()

found = False
key = 0
while(not found):
    line = puzzle + str(key)
    line = line.encode("utf-8")
    m.update(line)
    if((m.hexdigest())[:5] == "00000"):
        found = True
        print(key)
    else:
        key += 1
