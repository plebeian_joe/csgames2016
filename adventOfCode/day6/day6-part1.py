#!/usr/bin/python3.4

def parse_line(line):
    result = []
    if(line[:7] == "turn of"):
        result.append(0)

        line = line[9:]
        i = 0
        while(line[i] != ","):
            i += 1
        result.append(int(line[:i]))

        line = line[i+1:]
        i = 0
        while(line[i] != " "):
            i += 1
        result.append(int(line[:i]))

        line = line[i + 8:]
        i = 0
        while(line[i] != ","):
            i += 1
        result.append(int(line[:i]))

        line = line[i + 1:]
        i = 0
        while(line[i] != "\n"):
            i += 1
        result.append(int(line[:i]))


    elif(line[:7] == "turn on"):
        result.append(1)

        line = line[8:]
        i = 0
        while(line[i] != ","):
            i += 1
        result.append(int(line[:i]))

        line = line[i+1:]
        i = 0
        while(line[i] != " "):
            i += 1
        result.append(int(line[:i]))

        line = line[i + 8:]
        i = 0
        while(line[i] != ","):
            i += 1
        result.append(int(line[:i]))

        line = line[i + 1:]
        i = 0
        while(line[i] != "\n"):
            i += 1
        result.append(int(line[:i]))

    else:
        result.append(2)

        line = line[7:]
        i = 0
        while(line[i] != ","):
            i += 1
        result.append(int(line[:i]))

        line = line[i+1:]
        i = 0
        while(line[i] != " "):
            i += 1
        result.append(int(line[:i]))

        line = line[i + 8:]
        i = 0
        while(line[i] != ","):
            i += 1
        result.append(int(line[:i]))

        line = line[i + 1:]
        i = 0
        while(line[i] != "\n"):
            i += 1
        result.append(int(line[:i]))
    return result

def turn_on_off(grid, onoff, action):
    for i in range(action[0], action[2] + 1):
        for j in range(action[1], action[3] + 1):
            grid[i][j] = onoff
    return grid

def toggle(grid, action):
    for i in range(action[0], action[2] + 1):
        for j in range(action[1], action[3] + 1):
            if(grid[i][j] == 1):
                grid[i][j] = 0
            else:
                grid[i][j] = 1
    return grid

grid = [[0 for x in range(1000)] for x in range(1000)]

nb_lite_on = 0

f = open("input.txt", "r")
lines = f.readlines()
f.close()

for line in lines:
    action = parse_line(line)
    if(action[0] == 0 or action[0] == 1):
        grid = turn_on_off(grid, action[0], action[1:])
    else:
        toggle(grid, action[1:])

for i in range(1000):
    for j in range(1000):
        if grid[i][j] == 1:
            nb_lite_on += 1

print(nb_lite_on)
