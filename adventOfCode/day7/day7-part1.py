#!/usr/bin/python3.4

class Operation:

    def __init__(self, var1, notvar1, var2, notvar2, op, el):
        self.var1 = var1
        self.notvar1 = notvar1
        self.var2 = var2
        self.notvar2 = notvar2
        self.op = op
        self.el = el
        self.done = False

    def getVar1(self):
        return self.var1

    def getNotVar1(self):
        return self.notvar1

    def getVar2(self):
        return self.var2

    def getNotVar2(self):
        return self.notvar2

    def getOp(self):
        return self.op

    def getEl(self):
        return self.el

    def getDone(self):
        return self.done
    
    def is_basic(self):
        if(self.op == "=" and self.var1.isdigit()):
            return True
        else:
            return False

    def execute_basic(self, elements):
        elements[self.el] = self.var1
        self.done = True
        return elements

    def bitwise_and(self, elements):
        if(self.var1.isdigit()):
            v1 = int(self.var1)
        else:
            v1 = int(elements[self.var1])
        if(self.var2.isdigit()):
            v2 = int(self.var2)
        else:
            v2 = int(elements[self.var2])
        elements[self.el] = v1 & v2
        self.done = True
        return elements

    def bitwise_or(self, elements):
        if(self.var1.isdigit()):
            v1 = int(self.var1)
        else:
            v1 = int(elements[self.var1])
        if(self.var2.isdigit()):
            v2 = int(self.var2)
        else:
            v2 = int(elements[self.var2])
        elements[self.el] = v1 | v2
        self.done = True
        return elements

    def left_shift(self, elements):
        if(self.var1.isdigit()):
            v1 = int(self.var1)
        else:
            v1 = int(elements[self.var1])
        if(self.var2.isdigit()):
            v2 = int(self.var2)
        else:
            v2 = int(elements[self.var2])
        elements[self.el] = v1 << v2
        self.done = True
        return elements

    def right_shift(self, elements):
        if(self.var1.isdigit()):
            v1 = int(self.var1)
        else:
            v1 = int(elements[self.var1])
        if(self.var2.isdigit()):
            v2 = int(self.var2)
        else:
            v2 = int(elements[self.var2])
        elements[self.el] = v1 >> v2
        self.done = True
        return elements

    def execute_unary(self, elements):
        if(self.notvar1 == True):
            if(self.var1.isdigit()):
                elements[self.el] = ~(int(self.var1))
            else:
                if(self.var1 in elements):
                    elements[self.el] = ~(int(elements[self.var1]))
        else:
            if(self.var1.isdigit()):
                elements[self.el] = int(self.var1)
            else:
                if(self.var1 in elements):
                    elements[self.el] = int(elements[self.var1])

        self.done = True
        return elements

    def execute_binary(self, elements):
        if(self.op == "AND"):
            return self.bitwise_and(elements)
        elif(self.op == "OR"):
            return self.bitwise_or(elements)
        elif(self.op == "LSHIFT"):
            return self.left_shift(elements)
        elif(self.op == "RSHIFT"):
            return self.right_shift(elements)

    def execute(self, elements):
        if(not self.done):
            if((self.var1 in elements or self.var1.isdigit()) and self.var2 == ""):
                return self.execute_unary(elements)
            elif(self.var1 in elements or self.var1.isdigit()):
                if self.var2 in elements or self.var2.isdigit():
                    return self.execute_binary(elements)
        return elements

def parse_line(line):
    operands = line.split()
    if(len(operands) == 3):
        var1 = operands[0]
        notvar1 = False
        var2 = ""
        notvar2 = False
        op = "="
        el = operands[2]
    else:
        if(operands[0] == "NOT"):
            var1 = operands[1]
            notvar1 = True
            if(operands[2] == "->"):
                notvar2 = False
                var2 = ""
                op = ""
                el = operands[3]
            else:
                op = operands[2]
                if(operands[2] == "NOT"):
                    var2 = operands[4]
                    notvar2 = True
                    el = operands[6]
                else:
                    var2 = operands[3]
                    notvar2 = False
                    el = operands[5]
        else:
            var1 = operands[0]
            notvar1 = False
            op = operands[1]
            if(operands[2] == "NOT"):
                var2 = operands[3]
                notvar2 = True
                el = operands[5]
            else:
                var2 = operands[2]
                notvar2 = False
                el = operands[4]

    return (Operation(var1, notvar1, var2, notvar2, op, el))

operations = []
elements = {}

f = open("input.txt", "r")
lines = f.readlines()
f.close()

for line in lines:
    operations.append(parse_line(line))

for op in operations:
    if(op.is_basic()):
        elements = op.execute_basic(elements)

op_len = len(operations)
for i in range(op_len):
    for op in operations:
        elements = op.execute(elements)

print(elements["lx"])
print(elements["a"])
