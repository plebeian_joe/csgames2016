#!/usr/bin/python3.4

# calculate the XOR of all elements
def xor_sub_array(array):
    result = 0
    for i in range(len(array)):
        result ^= int(array[i])
    return result

nb_tests = input()

# for each test case, iterate through all the sub array possible
# and calculate the XOR result for every sub array.
for i in range(int(nb_tests)):
    nb_int = input()
    array = input().split()
    result = 0 
    for j in range(1, int(nb_int) + 1):
        for k in range(int(nb_int) - j + 1):
            result ^= xor_sub_array(array[k:k + j])
    
    print(result)
