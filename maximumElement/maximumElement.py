#!/usr/bin/python3.4

nb_line = input()

stack = []

max_value = 0

max_values = {}

stack_len = 0

for i in range(int(nb_line)):
    line = input()
    operands = line.split()
    if(len(operands) == 2):
        stack.append(int(operands[1]))
        stack_len += 1
        if(int(operands[1]) > max_value):
            max_values[stack_len] = int(operands[1])
            max_value = int(operands[1])
        else:
            max_values[stack_len] = max_value
    else:
        if(operands[0] == "2"):
            stack.pop()
            stack_len -= 1
            if stack_len in max_values:
                max_value = max_values[stack_len]
            else:
                max_value = 0
        else:
            print(max_values[stack_len])
