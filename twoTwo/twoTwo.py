#!/usr/bin/python3.4

import math

# Find if the number is power of 2 or not
def is_pow_2(nb):
    int_rep = int(math.log(nb, 2))
    float_rep = math.log(nb, 2)
    return True if (nb > 0 and int_rep == float_rep and int_rep < 800) else False

nb_tests = input()

results = []

for i in range(int(nb_tests)): # loop for the number of test cases
    test = input()
    test_len = len(test)
    total = 0
    for j in range(1, test_len + 1): # loop for the number of digits in the substring
        for k in range(test_len - j + 1): # itetare through every substring possible
            if test[k] != "0":
                if is_pow_2(int(test[k:k + j])):
                    total += 1
    
    results.append(total)


# printing the results
for res in results:
    print(res)
