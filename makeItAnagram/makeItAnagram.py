#!/usr/bin/python3.4

string_a = input()
string_b = input()

char_in_a = {}
char_in_b = {}

nb_deletion = 0

# couting the repetitions of each letter in string a
for char in string_a:
    if char in char_in_a:
        char_in_a[char] += 1
    else:
        char_in_a[char] = 1

# couting the repetitions of each letter in string b
for char in string_b:
    if char in char_in_b:
        char_in_b[char] += 1
    else:
        char_in_b[char] = 1

# getting the string in which there is more different characters
main_string = {}
other_string = {}
if len(char_in_b) > len(char_in_a):
    main_string = char_in_b
    other_string = char_in_a
else:
    main_string = char_in_a
    other_string = char_in_b

# counting the number of deletions needed from the string with more different characters
for char in main_string.keys():
    nb_in_main = main_string[char]
    if char in other_string:
        nb_in_other = other_string[char]
        other_string.pop(char)
    else:
        nb_in_other = 0
    nb_deletion += max(nb_in_main, nb_in_other) - min(nb_in_main, nb_in_other)

# counting the number of deletions needed from the other string
for char in other_string.keys():
    nb_deletion += other_string[char]

# printing the number of deletion needed
print(nb_deletion)
