#!/usr/bin/python3.4

nb_rocks = input()

rock = input()

# get the elements that can possibly be in all the rocks
elements_in_rock = {}
for el in rock:
    elements_in_rock[el] = 1


# iterate through all the rocks until all the rock have passed or
# there's no elements that can possibly be in all the rocks
i = 1
while(i < int(nb_rocks) and elements_in_rock):
    other_rock = input()
    elements_temp = {}
    for el in elements_in_rock:
        if other_rock.find(el) >= 0:
            elements_temp[el] = elements_in_rock[el]
    elements_in_rock = elements_temp
    i += 1

# print the number of elements that are in all the rocks
print(len(elements_in_rock))
