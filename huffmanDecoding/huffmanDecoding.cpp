void decode_huff2(node* root, string s, string* res, int* i)
{
  (*i)++;
  int debut = 1;
  if(root->left == NULL && root->right == NULL){
    res->push_back(root->data);
  } else {
    if(s.size() > 0){
      if(s.size() == 1) {
        debut = 0;
      }
      if(s.at(0) == '0'){
        decode_huff2(root->left, s.substr(debut), res, i);
      } else {
        decode_huff2(root->right, s.substr(debut), res, i);
      }
    }
  }
}

void decode_huff(node * root,string s)
{
  string* result = new string();
  int i; 
  int limit;
  while(s.size() > 0)
  {
    i = 0;
    if(s.at(0) == '0'){
      decode_huff2(root->left, s.substr(1), result, &i);
    } else {
      decode_huff2(root->right, s.substr(1), result, &i);
    }
    s = s.substr(i);
  }
  cout << *result;
}
